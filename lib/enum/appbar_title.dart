enum TitleAppBar {
  Title,
  SubTitle
}
extension TitleAppBarExtension on TitleAppBar {
  String get lable {
    switch(this){
      case TitleAppBar.Title:
        return 'Beyond Digital';
      case TitleAppBar.SubTitle:
        return 'Beyond Digital Test';
    }
  }
}