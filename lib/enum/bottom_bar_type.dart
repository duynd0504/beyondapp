import '../res.dart';

enum TabType { Home, Chat, Video, MyBeyond, IcBeyond }

extension TabTypeExtension on TabType {
  String get lable {
    switch (this) {
      case TabType.Home:
        return 'Home';
      case TabType.Chat:
        return 'Chat';
      case TabType.Video:
        return 'Video';
      case TabType.MyBeyond:
        return 'My Beyond';
      case TabType.IcBeyond:
        return '';
    }
  }

  String get unselectedIcon {
    switch (this) {
      case TabType.Home:
        return Res.ic_homeinactive;
      case TabType.Chat:
        return Res.ic_chat;
      case TabType.Video:
        return Res.ic_video;
      case TabType.MyBeyond:
        return Res.ic_mybeyond;
      case TabType.IcBeyond:
        return Res.ic_beyond;
    }
  }

  String get selectedIcon {
    switch (this) {
      case TabType.Home:
        return Res.ic_home;
      case TabType.Chat:
        return Res.ic_chatactive;
      case TabType.Video:
        return Res.ic_videoactive;
      case TabType.MyBeyond:
        return Res.ic_mybeyondactive;
      case TabType.IcBeyond:
        return Res.ic_beyond;
    }
  }
}
