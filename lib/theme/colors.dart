import 'dart:ui';

import 'package:flutter/material.dart';

class AppColors {
  AppColors._();
  static const Color unselectBottomBarColor = Color(0xffB2C2CC);
  static const Color textBottomBarColor = Color(0xff333333);
  static const Color textTitleColorAppBar = Color(0xff000000);
  static const Color icSeachColor = Color(0xff707A83);
  static const Color bgColor = Color(0xfff7f7f7);
  static const Color bgColorprimary = Colors.white;
  //--------------------------------------------------------
  static MaterialColor primaryColor = MaterialColor(
    0xff2E9E4C,
    <int, Color>{
      50: Color(0xFFF9FDFA),
      100: Color(0xFFF9FDFA),
      200: Color(0xFFE2F9E4),
      300: Color(0xFFB7E4BB),
      400: Color(0xFF62BC6B),
      500: Color(0xff2E9E4C),
      600: Color(0xFF1F7A38),
      700: Color(0xFF206031),
      800: Color(0xFF194D28),
      900: Color(0xFF164122),
    },
  );
  static MaterialColor neutralColor = MaterialColor(
    0xff899F8F,
    <int, Color>{
      50: Color(0xFFE8F5E9),
      100: Color(0xFFFCFDFC),
      200: Color(0xFFDDE3DF),
      300: Color(0xFFC1CDC4),
      400: Color(0xFFA5B6AA),
      500: Color(0xff899F8F),
      600: Color(0xFF6E8775),
      700: Color(0xFF576B5C),
      800: Color(0xFF404F44),
      900: Color(0xFF222A24),
    },
  );

  static MaterialColor alertColor = MaterialColor(
    0xff899F8F,
    <int, Color>{
      50: Color(0xFFFFF5F5),
      100: Color(0xFFFFF5F5),
      200: Color(0xFFFFE5E6),
      300: Color(0xFFFEB8B8),
      400: Color(0xFFF46F6F),
      500: Color(0xffDC3C3C),
      600: Color(0xFFBD2828),
      700: Color(0xFFA02222),
      800: Color(0xFF7E1B1B),
      900: Color(0xFF541212),
    },
  );
  static const Color accentColor = Color(0xFF2E9E4C);
  static const Color lightGrey = Color(0xFFBFBFBF);
  static const Color facebook = Color(0xFF3b5998);
  static const Color grey = Color(0xffdbdbdb);
  static const Color inactiveButton = Color(0xFFEAEAEA);
  static const Color activeButton = Color(0xFF2E9E4C);
  static const Color inActiveButton = const Color(0xffBBBBBB);

  static const Color textDefault = Color(0xFF3D4B5C);
  static const Color pickerDefaultColor = Color(0xFF262626);
  static const Color textGray = Color(0xFF969696);
  static const Color borderGray = Color(0xFF7C7D80);
  static const Color textBlack = Color(0xFF29323D);
  static const Color backgroundDarkGray = Color(0xFF24272C);
  static const Color backgroundGray = Color(0xFF7C7D80);
  static const Color textWhite = Color(0xFFFFFFFF);
  static const Color backgroundWhite = Color(0xFFFFFFFF);
  static const Color backgroundBlack = Color(0xFF111315);
  static const Color progressBarPlayedColor = Color(0xFFFEC52E);
  static const Color progressBarHandleColor = Color(0xFFFEC52E);
  static const Color progressBarBufferedColor = Color(0xB3FFFFFF);
  static const Color progressBarBackgroundColor = Color(0xFF24272C);
  static const Color backgroundChip = Color(0xFF24272C);
  static const Color textRed = Color(0xFFFF0000);
  static const Color titleTextColor = const Color(0xff48668a);
  static const Color defaultTextColor = const Color(0xff1f3ca8);
  static const Color bgInputColor = const Color(0xffe7effa);
  static const Color searchBarColor = const Color(0xffe7effa);
  static const Color dividerColor = const Color(0xffe7effa);
  static const Color textSelectedColor = const Color(0xff0057BD);
  static const Color textUnSelectedColor = const Color(0xff52647A);
  static const Color textInputColor = const Color(0xff314768);
  static const Color errorInputColor = const Color(0xfff75252);
  static const Color redTextColor = const Color(0xfff75252);
  static const Color blueTextColor = const Color(0xff326fe3);
  static const Color loadingIndicatorColor = const Color(0xff326fe3);
  static const Color radioActiveColor = const Color(0xff326fe3);
  static const Color dividerGray = const Color(0xffC2CBD6);
  static const Color disableColor = const Color(0xffBCBCBC);
  static const Color disableTextColor = const Color(0xff747474);
  static const Color indicatorActiveColor = const Color(0xff558aff);
  static const Color indicatorInActiveColor = const Color(0xffc9d3de);
  static const Color iconDownColor = const Color(0xff6f84a4);
  static const Color iconDefaultColor = const Color(0xff667D99);
  static const Color iconActiveColor = const Color(0xFF0057BD);
  static const Color hintTextColor = const Color(0xff8c8c8c);
  static const Color buttonTranslucent = const Color(0x99333333);
  static const Color scaffoldBackground = const Color(0xFFFFFFFF);
  static const Color fillSearchBar = const Color(0xFFF5F6F5);
  static const Color activeIndicator = const Color(0XFFFAFAFA);
  static const Color inactiveIndicator = const Color(0xFF767676);

  static const Color textDark = Color(0xFF14191F);
  static const Color darkBlue = const Color(0xFF0057BD);
  static const Color textBlue = const Color(0xFF006EF0);
  static const Color subTitleTex = const Color(0xFF3D4B5C);

  static MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    final swatch = <int, Color>{};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    strengths.forEach((strength) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    });
    return MaterialColor(color.value, swatch);
  }
}
