import 'dart:ui';
import 'package:flutter/material.dart';
import 'colors.dart';

class AppStyle {
  AppStyle._();
  static const Roboto = "Roboto";
  static const textTheme = TextTheme(
    bodyText1:  TextStyle(fontFamily: Roboto, fontSize: 24 , ),
    bodyText2:  TextStyle(fontFamily: Roboto, fontSize: 18 , ),
    headline6:  TextStyle(fontFamily: Roboto, fontSize: 16, fontWeight: FontWeight.bold, ),
    headline5:  TextStyle(fontFamily: Roboto, fontSize: 18, fontWeight: FontWeight.bold, ),
    headline4:  TextStyle(fontFamily: Roboto, fontSize: 24, fontWeight: FontWeight.bold, ),
    headline3:  TextStyle(fontFamily: Roboto, fontSize: 28, fontWeight: FontWeight.bold, ),
    headline2:  TextStyle(fontFamily: Roboto, fontSize: 32, fontWeight: FontWeight.w900, ),
    headline1:  TextStyle(fontFamily: Roboto, fontSize: 36, fontWeight: FontWeight.bold),

  );
}