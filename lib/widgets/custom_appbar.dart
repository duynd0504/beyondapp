import 'package:beyond/enum/appbar_title.dart';
import 'package:beyond/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../res.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String imageUrlTitle;
  const CustomAppBar(this.imageUrlTitle, {Key? key}) : super(key: key);

  Size get preferredSize => Size.fromHeight(60.0);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: AppColors.bgColorprimary,
      centerTitle: true,
      elevation: 2,
      leading: Image.asset(
        imageUrlTitle,
        height: 24,
        width: 48,
        alignment: Alignment.centerRight,
      ),
      title: Text(
        //get value title of appbar
        TitleAppBar.Title.lable,
        style: Theme.of(context).textTheme.bodyText2?.copyWith(
            fontSize: 18,
            color: AppColors.textTitleColorAppBar,
            fontWeight: FontWeight.w700),
      ),
      actions: [
        IconButton(
          color: AppColors.icSeachColor,
          onPressed: () {},
          icon: SvgPicture.asset(Res.ic_search),
        ),
      ],
    );
  }
}
