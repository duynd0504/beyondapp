import 'package:flutter/widgets.dart';

class CircleView extends StatelessWidget {
  final Color color;
  final double size;

  const CircleView({Key? key, required this.color, required this.size})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: size,
        height: size,
        decoration: BoxDecoration(shape: BoxShape.circle, color: color));
  }
}
