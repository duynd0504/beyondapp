import 'package:beyond/theme/colors.dart';
import 'package:beyond/widgets/indicator.dart';
import 'package:flutter/material.dart';

import '../res.dart';

class IntroPageItem extends StatefulWidget {
  // final String title;
  // final String content;

  const IntroPageItem({
    Key? key,
    // required this.title,
    // required this.content,
  }) : super(key: key);

  @override
  _IntroPageItemState createState() => _IntroPageItemState();
}

class _IntroPageItemState extends State<IntroPageItem>
    with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
          text: TextSpan(
            text: 'Welcome to the jungle',
            style: Theme.of(context)
                .textTheme
                .bodyText2!
                .copyWith(fontSize: 32, color: Colors.black),
          ),
        ),
        Text(
          'The story behind',
          style: Theme.of(context)
              .textTheme
              .bodyText2!
              .copyWith(fontSize: 23, color: Colors.black),
        ),
        Text(
          'the mini-game',
          style: Theme.of(context)
              .textTheme
              .bodyText2!
              .copyWith(fontSize: 23, color: Colors.black),
          textAlign: TextAlign.left,
        ),
        SizedBox(
          height: 50,
        ),
        SizedBox(
          height: 16,
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
