import 'package:beyond/theme/colors.dart';
import 'package:flutter/material.dart';

import 'circle_view.dart';

class Indicators extends StatelessWidget {
  final int currentPosition;

  const Indicators({Key? key, required this.currentPosition}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: 10,
        child: ListView.separated(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemBuilder: (_, int index) {
              final color = (index == currentPosition)
                  ? AppColors.activeIndicator
                  : AppColors.inactiveIndicator;
              return CircleView(
                color: color,
                size: 8,
              );
            },
            separatorBuilder: (_, int index) {
              return SizedBox(
                width: 8,
              );
            },
            itemCount: 3),
      ),
    );
  }
}
