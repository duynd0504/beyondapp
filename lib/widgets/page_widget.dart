import 'package:flutter/material.dart';
import 'context_extension.dart';

class PageWidget extends StatelessWidget {
  final Widget child;
  final Widget? appbar;

  const PageWidget({Key? key, required this.child})
      : appbar = null,
        super(key: key);

  const PageWidget.withAppbar(
      {Key? key, required this.child, required this.appbar})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            context.hideKeyboard();
          },
          child: appbar == null
              ? child
              : Column(
                  children: [appbar!, child],
                )),
    );
  }
}
