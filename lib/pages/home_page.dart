import 'package:beyond/enum/appbar_title.dart';
import 'package:beyond/enum/bottom_bar_type.dart';
import 'package:beyond/theme/colors.dart';
import 'package:beyond/widgets/button.dart';
import 'package:beyond/widgets/custom_appbar.dart';
import 'package:beyond/widgets/indicator.dart';
import 'package:beyond/widgets/intro_item.dart';
import 'package:beyond/widgets/page_widget.dart';
import 'package:flutter/material.dart';
import 'package:beyond/res.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late int _currentIndexPage = 0;
  late int _currentIndex = 0;
  PageController pageController = PageController();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: CustomAppBar(Res.img),
      body: PageWidget(
        child: Column(
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
              child: Container(
                height: 450,
                child: Material(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.black26,
                  //chuyen doi hinh anh thay container bang image
                  child: Container(
                    color: Colors.white24,
                    child: Column(
                      children: [
                        Expanded(
                          child: PageView.builder(
                            controller: pageController,
                            itemCount: 3,
                            onPageChanged: (position) {
                              setState(() {
                                _currentIndexPage = position;
                              });
                            },
                            itemBuilder: (BuildContext _, int index) {
                              return Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(12, 270, 30, 0),
                                child: IntroPageItem(),
                              );
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(bottom: 9),
                          child: Indicators(
                            currentPosition: _currentIndexPage,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            //
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Branded Mini-game',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2!
                        .copyWith(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  // ignore: deprecated_member_use
                  FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: Colors.black)),
                    color: Colors.white,
                    textColor: Colors.black,
                    padding: EdgeInsets.all(8.0),
                    onPressed: () {},
                    child: Text("View All",
                        style: Theme.of(context).textTheme.bodyText2!.copyWith(
                            fontSize: 16, fontWeight: FontWeight.w700)),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 5,
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        backgroundColor: AppColors.bgColorprimary,
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 15,
        unselectedFontSize: 14,
        selectedItemColor: AppColors.textBottomBarColor,
        unselectedItemColor: AppColors.unselectBottomBarColor,
        items: [
          BottomNavigationBarItem(
            activeIcon: SvgPicture.asset(TabType.Home.selectedIcon),
            icon: SvgPicture.asset(TabType.Home.unselectedIcon),
            label: TabType.Home.lable,
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(TabType.Chat.unselectedIcon),
            label: TabType.Chat.lable,
            activeIcon: SvgPicture.asset(TabType.Chat.selectedIcon),
          ),
          BottomNavigationBarItem(
            activeIcon: SvgPicture.asset(TabType.Video.selectedIcon),
            icon: SvgPicture.asset(TabType.Video.unselectedIcon),
            label: TabType.Video.lable,
          ),
          BottomNavigationBarItem(
            activeIcon: SvgPicture.asset(TabType.MyBeyond.selectedIcon),
            icon: SvgPicture.asset(TabType.MyBeyond.unselectedIcon),
            label: TabType.MyBeyond.lable,
          ),
          BottomNavigationBarItem(
            icon: Container(
              margin: const EdgeInsets.only(top: 15),
              child: Image.asset(Res.img),
            ),
            label: '',
          )
        ],
      ),
    );
  }
}

Widget buildIndicator(bool isActive, Size size) {
  return AnimatedContainer(
    duration: Duration(microseconds: 300),
    curve: Curves.bounceInOut,
    height: 10,
    margin: const EdgeInsets.symmetric(horizontal: 10),
    width: isActive ? size.width * 1 / 5 : 35,
    decoration: BoxDecoration(
        color: isActive ? AppColors.accentColor : AppColors.lightGrey,
        borderRadius: BorderRadius.all(Radius.circular(14)),
        boxShadow: [
          BoxShadow(color: Colors.black38, offset: Offset(2, 3), blurRadius: 3)
        ]),
  );
}
